<?php
/**
 * Scandiweb_Store
 *
 * @category    Scandiweb
 * @package     Scandiweb_Store
 * @author      Sandis Bogacenkovs <sandis.bogacenkovs@scandiweb.com>
 * @copyright   Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Store\Setup\Patch\Data;

use Magento\Directory\Model\ResourceModel\Currency;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\View\Design\Theme\ThemeProviderInterface;
use Magento\Store\Api\StoreRepositoryInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store as StoreModel;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\WebsiteFactory;
use Magento\Theme\Model\Config;
use Magento\Store\Model\ResourceModel\Website\CollectionFactory;

/**
 * Class StorePatch
 *
 * @package Scandiweb\Store\Setup\Patch\Data
 */
class StorePatch implements DataPatchInterface
{
    const THEME_PATH = 'scandi';
    const BASE_STORE_WEBSITE_CODE = 'base';
    const CURRENCY_OPT = 'currency/options/';
    const DEFAULT_SCOPE = 0;
    const DEFAULT_STORE_SCOPE = 1;
    const GBP_RATE = 0.9;

    /**
     * @var GroupFactory
     */
    private $groupFactory;

    /**
     * @var StoreFactory
     */
    private $storeFactory;

    /**
     * @var Store
     */
    private $storeResourceModel;

    /**
     * @var WebsiteFactory
     */
    private $websiteFactory;

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var ConfigInterface
     */
    private $configInterface;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var ThemeProviderInterface
     */
    private $themeProvider;

    /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    /**
     * @var WriterInterface
     */
    private $writerInterface;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var CollectionFactory
     */
    private $websiteCollectionFactory;

    /**
     * StorePatch constructor.
     *
     * @param GroupFactory $groupFactory
     * @param StoreFactory $storeFactory
     * @param WebsiteFactory $websiteFactory
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ConfigInterface $configInterface
     * @param ThemeProviderInterface $themeProvider
     * @param Config $config
     * @param StoreRepositoryInterface $storeRepository
     * @param Currency $currency
     * @param StoreManagerInterface $storeManager
     * @param WriterInterface $writerInterface
     * @param Store $storeResourceModel
     * @param CollectionFactory $websiteCollectionFactory
     */
    public function __construct(
        GroupFactory $groupFactory,
        StoreFactory $storeFactory,
        WebsiteFactory $websiteFactory,
        ModuleDataSetupInterface $moduleDataSetup,
        ConfigInterface $configInterface,
        ThemeProviderInterface $themeProvider,
        Config $config,
        StoreRepositoryInterface $storeRepository,
        Currency $currency,
        StoreManagerInterface $storeManager,
        WriterInterface $writerInterface,
        Store $storeResourceModel,
        CollectionFactory $websiteCollectionFactory
    ) {
        $this->groupFactory = $groupFactory;
        $this->storeFactory = $storeFactory;
        $this->storeManager = $storeManager;
        $this->websiteFactory = $websiteFactory;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->configInterface = $configInterface;
        $this->storeManager = $storeManager;
        $this->themeProvider = $themeProvider;
        $this->config = $config;
        $this->storeRepository = $storeRepository;
        $this->currency = $currency;
        $this->writerInterface = $writerInterface;
        $this->storeResourceModel = $storeResourceModel;
        $this->websiteCollectionFactory = $websiteCollectionFactory;
    }

    /**
     * Creates German Store
     *
     * @throws AlreadyExistsException
     * @throws LocalizedException
     */
    public function createStore()
    {
        $default_attributes = [
            [
                'store_code' => 'usd_website_store',
                'store_name' => 'German Store',
                'root_category_id' => '2',
                'is_active' => '1',
                'default_group_id' => '1',
                'type' => 'german'
            ]
        ];

        foreach ($default_attributes as $attribute) {
            /** @var  StoreModel $store */
            $store = $this->storeFactory->create();
            $store->load($attribute['store_code']);

            if (!$store->getId()) {
                $website = $this->websiteCollectionFactory->create()
                    ->addFieldToFilter('code', self::BASE_STORE_WEBSITE_CODE)
                    ->getFirstItem();

                $store->setCode($attribute['store_code']);
                $store->setName($attribute['store_name']);
                $store->setWebsite($website);
                $store->setGroupId($attribute['default_group_id']);
                $store->setData('is_active', $attribute['is_active']);
                $this->storeResourceModel->save($store);
                $themePath = 'frontend' . DIRECTORY_SEPARATOR . self::THEME_PATH . DIRECTORY_SEPARATOR . $attribute['type'];
                $storeId = $store->getStoreId();
                $themeId = $this->themeProvider->getThemeByFullPath($themePath)->getThemeId();
                $this->setThemes($storeId, $themeId);
            }
        }
    }

    /**
     * Edit Default Store
     *
     * @throws AlreadyExistsException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function editDefaultStore()
    {
        $defaultStore = $this->storeRepository->get('default');
        $storeId = $defaultStore->getId();
        $themePath = 'frontend' . DIRECTORY_SEPARATOR . self::THEME_PATH . DIRECTORY_SEPARATOR . 'default';
        $themeId = $this->themeProvider->getThemeByFullPath($themePath)->getThemeId();

        $defaultStore->setName('Europe Store');
        $this->setThemes($storeId, $themeId);
        $this->currency->saveRates(['EUR' => ['GBP' => self::GBP_RATE]]);

        $this->storeResourceModel->save($defaultStore);
    }

    /**
     * Set Themes
     *
     * @param $storeId
     * @param $themeId
     */
    public function setThemes($storeId, $themeId)
    {
        $this->configInterface->saveConfig(
            'design/theme/theme_id',
            $themeId,
            ScopeInterface::SCOPE_STORES,
            $storeId
        );
    }

    /**
     * Set default currencies
     */
    public function setDefaultCurrencies()
    {
        $this->configInterface->saveConfig(
            self::CURRENCY_OPT . 'base',
            'EUR',
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            self::DEFAULT_SCOPE
        );
        $this->configInterface->saveConfig(
            self::CURRENCY_OPT . 'default',
            'EUR',
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            self::DEFAULT_SCOPE
        );
        $this->configInterface->saveConfig(
            self::CURRENCY_OPT . 'allow',
            'EUR,GBP',
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            self::DEFAULT_SCOPE
        );
    }

    /**
     * Set Store currencies
     *
     * @throws NoSuchEntityException
     */
    public function setStoreCurrencies()
    {
        $this->setDefaultCurrencies();
        $stores = $this->storeManager->getStore(true);

        if ($stores->getCode() == 'default') {
            $this->configInterface->saveConfig(
                self::CURRENCY_OPT . 'allow',
                'GBP',
                ScopeInterface::SCOPE_STORES,
                self::DEFAULT_STORE_SCOPE
            );
            $this->configInterface->saveConfig(
                self::CURRENCY_OPT . 'default',
                'GBP',
                ScopeInterface::SCOPE_STORES,
                self::DEFAULT_STORE_SCOPE
            );
        }
    }

    /**
     * Apply Store Patch
     *
     * @return StorePatch|void
     * @throws AlreadyExistsException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $this->createStore();
        $this->editDefaultStore();
        $this->setStoreCurrencies();
        $this->moduleDataSetup->getConnection()->endSetup();

        // Removes .html suffix from product and category pages
        $this->writerInterface->save(
            'catalog/seo/product_url_suffix',
            '',
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            self::DEFAULT_SCOPE
        );
        $this->writerInterface->save(
            'catalog/seo/category_url_dsuffix',
            '',
            ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            self::DEFAULT_SCOPE
        );
    }

    /**
     * Get Aliases
     *
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Get Dependencies
     *
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }
}
