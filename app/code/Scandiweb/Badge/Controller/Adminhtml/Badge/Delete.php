<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Controller\Adminhtml\Badge;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Scandiweb\Badge\Model\Badge;

/**
 * Class Delete
 *
 * @package Scandiweb\Badge\Controller\Adminhtml\Badge
 */
class Delete extends Action
{
    /**
     * @var Badge
     */
    protected $model;

    /**
     * Delete constructor.
     *
     * @param Context $context
     * @param Badge $model
     */
    public function __construct(
        Context $context,
        Badge $model
    ) {
        parent::__construct($context);
        $this->model = $model;
    }

    /**
     * Get is allowed
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Scandiweb_Badge::badge_delete');
    }

    /**
     * Delete action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($id) {
            try {
                $model = $this->_objectManager->create(Badge::class);
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccessMessage(__('Selected Badge deleted'));

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());

                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $id]);
            }
        }

        $this->messageManager->addErrorMessage(__('An error occurred while deleting this badge'));

        return $resultRedirect->setPath('*/*/');
    }
}
