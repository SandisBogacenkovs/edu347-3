<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Controller\Adminhtml\Badge;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Scandiweb\Badge\Controller\Adminhtml\Badge;
use Scandiweb\Badge\Model\Badge as BadgeModel;
use Scandiweb\Badge\Model\ResourceModel\Badge\CollectionFactory;
use Scandiweb\Badge\Model\BadgeFactory;

/**
 * Class Edit
 *
 * @package Scandiweb\Badge\Controller\Adminhtml\Badge
 */
class Edit extends Badge
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var BadgeFactory
     */
    private $badgeFactory;

    /**
     * Edit constructor.
     *
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     * @param PageFactory $resultPageFactory
     * @param BadgeFactory $badgeFactory
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        PageFactory $resultPageFactory,
        badgeFactory $badgeFactory
    ) {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->badgeFactory = $badgeFactory;
    }

    /**
     * Edit action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        if ($id) {
            /* @var BadgeModel $badge */
            $badge = $this->collectionFactory->create()
                ->addFieldToFilter('entity_id', $id)
                ->getFirstItem();

            if (!isset($badge)) {
                $this->messageManager->addErrorMessage(__('This Badge no longer exists.'));

                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Badge') : __('New Badge'),
            $id ? __('Edit Badge') : __('New Badge')
        );

        $resultPage->getConfig()->getTitle()->prepend(__('Badges'));
        $resultPage->getConfig()->getTitle()->prepend(
            $id ? __('Edit Badge %1', $id) : __('New Badge')
        );

        return $resultPage;
    }
}
