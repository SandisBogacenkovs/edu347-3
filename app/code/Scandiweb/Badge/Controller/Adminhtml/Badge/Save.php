<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Controller\Adminhtml\Badge;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Scandiweb\Badge\Api\Data\BadgeInterface;
use Scandiweb\Badge\Model\Badge;
use Scandiweb\Badge\Model\BadgeFactory;
use Scandiweb\Badge\Model\ResourceModel\Badge as BadgeResource;
use Scandiweb\Badge\Model\ResourceModel\Badge\CollectionFactory as BadgeCollection;


/**
 * Class Save
 *
 * @package Scandiweb\Badge\Controller\Adminhtml\Badge
 */
class Save extends Action implements HttpPostActionInterface
{

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var BadgeFactory
     */
    private $badgeFactory;

    /**
     * @var BadgeResource
     */
    private $badgeResource;

    /**
     * @var Badge
     */
    private $badge;

    /**
     * @var BadgeCollection
     */
    private $badgeCollection;

    /**
     * Save constructor.
     *
     * @param Context $context
     * @param BadgeFactory $badgeFactory
     * @param BadgeResource $badgeResource
     * @param Badge $badge
     * @param DataPersistorInterface $dataPersistor
     * @param BadgeCollection $badgeCollection
     */
    public function __construct(
        Context $context,
        BadgeFactory $badgeFactory,
        BadgeResource $badgeResource,
        Badge $badge,
        DataPersistorInterface $dataPersistor,
        BadgeCollection $badgeCollection
    ) {
        parent::__construct($context);
        $this->badgeFactory = $badgeFactory;
        $this->badgeResource = $badgeResource;
        $this->badge = $badge;
        $this->dataPersistor = $dataPersistor;
        $this->badgeCollection = $badgeCollection;
    }

    /**
     * Save action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var Redirect $resultRedirect */
        $redirect = $this->resultRedirectFactory->create();

        $redirect->setPath('*/*/');

        if ($data) {
            $id = $this->getRequest()->getParam(BadgeInterface::ENTITY_ID);

            if (!$id) {
                /** @var Badge $badge */
                $badge = $this->badgeFactory->create();
            } else {
                $badge = $this->badgeCollection->create()
                    ->addFieldToFilter(BadgeInterface::ENTITY_ID, $id)
                    ->getFirstItem();
            }

            $image = $data['img'][0]['name'];

            $badge->setName($data['name']);
            $badge->setStatus($data['status']);
            $badge->setIdentifier($data['identifier']);
            $badge->setImg($image);

            try {
                $this->badgeResource->save($badge);
                $this->messageManager->addSuccessMessage('Badge Saved Successfully!');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        }

        return $redirect;
    }
}
