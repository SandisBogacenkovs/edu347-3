<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;

/**
 * Class Badge
 *
 * @package Scandiweb\Badge\Controller\Adminhtml
 */
abstract class Badge extends Action
{
    const ADMIN_RESOURCE = 'Scandiweb_Badge::top_level';

    /**
     * Constructor
     *
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param Page $resultPage
     *
     * @return Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Scandiweb'), __('Scandiweb'))
            ->addBreadcrumb(__('Badge'), __('Badge'));

        return $resultPage;
    }
}
