<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Scandiweb\Badge\Model\ResourceModel\Badge\CollectionFactory as BadgeCollection;

/**
 * Class Badge
 *
 * @package Scandiweb\Badge\Helper
 */
class Badge extends AbstractHelper
{
    const BADGE_IMAGES_PATH = 'badge/tmp/icon/';

    /**
     * @var BadgeCollection
     */
    private $badgeCollectionFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Badge constructor.
     *
     * @param BadgeCollection $badgeCollectionFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        BadgeCollection $badgeCollectionFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->badgeCollectionFactory = $badgeCollectionFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * Get badge by ID
     *
     * @param $badgeId
     *
     * @return bool|DataObject
     */
    public function getBadge($badgeId)
    {
        $badge = $this->badgeCollectionFactory->create()
            ->addFieldToFilter('entity_id', $badgeId)
            ->getFirstItem();

        if (isset($badge) && $badge->getStatus() === '1') {
            return $badge;
        }

        return false;
    }

    /**
     * Get badge image URL
     *
     * @param $badge
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function getBadgeImageUrl($badge)
    {
        $badgeImageUrl = '';

        if (isset($badge) && $badge->getId()) {
            $mediaUrl = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
            $badgeImageUrl = $mediaUrl . self::BADGE_IMAGES_PATH . $badge->getImg();
        }

        return $badgeImageUrl;
    }
}
