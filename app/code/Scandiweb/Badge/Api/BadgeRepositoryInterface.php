<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Scandiweb\Badge\Api\Data\BadgeInterface;

/**
 * Interface BadgeRepositoryInterface
 *
 * @package Scandiweb\Badge\Api
 */
interface BadgeRepositoryInterface
{
    /**
     * Save Badge
     *
     * @param BadgeInterface $badge
     *
     * @return BadgeInterface
     * @throws LocalizedException
     */
    public function save(BadgeInterface $badge);

    /**
     * Retrieve Badge
     *
     * @param string $badgeId
     *
     * @return BadgeInterface
     * @throws LocalizedException
     */
    public function get($badgeId);

    /**
     * Get List
     *
     * @param SearchCriteriaInterface $searchCriteria
     *
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Badge
     *
     * @param BadgeInterface $badge
     *
     * @return bool
     * @throws LocalizedException
     */
    public function delete(BadgeInterface $badge);

    /**
     * Delete Badge by ID
     *
     * @param string $badgeId
     *
     * @return bool
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($badgeId);
}
