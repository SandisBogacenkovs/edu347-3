<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface BadgeInterface
 *
 * @package Scandiweb\Badge\Api\Data
 */
interface BadgeInterface extends ExtensibleDataInterface
{
    const ENTITY_ID = 'entity_id';
    const NAME = 'name';
    const IDENTIFIER = 'identifier';
    const STATUS = 'status';
    const IMG = 'img';

    /**
     * Get entity_id
     *
     * @return string|null
     */
    public function getEntityId();

    /**
     * Set entity_id
     *
     * @param string $entityId
     *
     * @return BadgeInterface
     */
    public function setEntityId($entityId);

    /**
     * Get identifier
     *
     * @return string|null
     */
    public function getIdentifier();

    /**
     * Set identifier
     *
     * @param string $entityId
     *
     * @return BadgeInterface
     */
    public function setIdentifier($identifier);

    /**
     * Get Badge Name
     *
     * @return string
     */
    public function getName();

    /**
     * Set Badge Name
     *
     * @param $name
     *
     * @return BadgeInterface
     */
    public function setName($name);

    /**
     * Get Badge Status
     *
     * @return int
     */
    public function getStatus();

    /**
     * Set Badge Status
     *
     * @param $status
     *
     * @return BadgeInterface
     */
    public function setStatus($status);

    /**
     * Get Badge Image
     *
     * @return string
     */
    public function getImg();

    /**
     * Set Badge Image
     *
     * @param $img
     *
     * @return BadgeInterface
     */
    public function setImg($img);
}
