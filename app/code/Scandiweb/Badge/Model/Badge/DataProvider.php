<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Model\Badge;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Scandiweb\Badge\Model\Badge;
use Scandiweb\Badge\Model\ResourceModel\Badge\Collection;
use Scandiweb\Badge\Model\ResourceModel\Badge\CollectionFactory;

/**
 * Class DataProvider
 *
 * @package Scandiweb\Badge\Model\Badge
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var $loadedData
     */
    protected $loadedData;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param StoreManagerInterface $storeManager
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->storeManager = $storeManager;
    }

    /**
     * Get data
     *
     * @return array
     * @throws NoSuchEntityException
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        /** @var Badge $badge */
        foreach ($items as $badge) {
            $badgeImage = $badge->getImg();
            $this->loadedData[$badge->getId()] = $badge->getData();

            if ($badgeImage) {
                $this->loadedData[$badge->getId()]['img'] = [
                    [
                        'name' => $badgeImage,
                        'url' => $this->getMediaUrl() . '/' . $badgeImage
                    ]
                ];
            }
        }


        return $this->loadedData;
    }

    /**
     * Get Full URL
     *
     * @return string
     * @throws NoSuchEntityException
     */
    public function getMediaUrl()
    {
        return $this->storeManager->getStore()
                ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . ImageUploader::IMAGE_TMP_PATH;
    }
}
