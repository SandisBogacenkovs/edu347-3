<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Model\Attributes;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Scandiweb\Badge\Model\ResourceModel\Badge\CollectionFactory;

/**
 * Class ProductBadgeSource
 *
 * @package Scandiweb\Badge\Model\Attributes
 */
class ProductBadgeSource extends AbstractSource
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * ProductBadgeSource constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get all options
     *
     * @return array|array[]
     */
    public function getAllOptions()
    {
        $options = [];
        $badgeCollection = $this->collectionFactory->create()
            ->addFieldToFilter('status', 1)
            ->getItems();

        if (empty($badgeCollection)) {
            $options[] = [
                'label' => 'No badges available',
                'value' => null,
            ];
        } else {
            $options[] =  [
                'label' => 'Select Badge',
                'value' => null,
            ];

            foreach ($badgeCollection as $badge) {
                $options[] = [
                    'label' => $badge['name'],
                    'value' => $badge['entity_id'],
                ];
            }
        }

        return $options;
    }

    /**
     * Get options text
     *
     * @param int|string $value
     *
     * @return bool|string
     */
    public function getOptionText($value)
    {
        if ($value == null) {
            return false;
        }

        $badgeCollection = $this->collectionFactory->create();

        return $badgeCollection->getItemById($value)->getDataByKey('name');
    }
}
