<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 *
 * @package Scandiweb\Badge\Model\Config\Source
 */
class Status implements OptionSourceInterface
{
    const ENABLE_VALUE = 1;
    const DISABLE_VALUE = 0;

    /**
     * To options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::ENABLE_VALUE,
                'label' => __('Enabled'),
            ],
            [
                'value' => self::DISABLE_VALUE,
                'label' => __('Disabled'),
            ],
        ];
    }
}
