<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Model\ResourceModel\Badge;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Scandiweb\Badge\Model\Badge as BadgeModel;
use Scandiweb\Badge\Model\ResourceModel\Badge as BadgeResource;

/**
 * Class Collection
 *
 * @package Scandiweb\Badge\Model\ResourceModel\Badge
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = BadgeModel::ENTITY_ID;

    /**
     * Initialize resource collection
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(BadgeModel::class, BadgeResource::class);
    }
}
