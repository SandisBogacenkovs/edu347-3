<?php
/**
 * Scandiweb_Badge
 *
 * @category  Scandiweb
 * @package   Scandiweb_Badge
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\Badge\Model;

use Magento\Framework\Model\AbstractModel;
use Scandiweb\Badge\Api\Data\BadgeInterface;
use Scandiweb\Badge\Model\ResourceModel\Badge as BadgeResource;

/**
 * Class Badge
 *
 * @package Scandiweb\Badge\Model
 */
class Badge extends AbstractModel implements BadgeInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(BadgeResource::class);
    }

    /**
     * Get ID
     *
     * @return array|mixed|string|null
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int|string $entityId
     *
     * @return BadgeInterface|Badge
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get Name
     *
     * @return array|mixed|string|null
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Set Name
     *
     * @param $name
     *
     * @return BadgeInterface|Badge
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get Status
     *
     * @return array|int|mixed|null
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Set Status
     *
     * @param $status
     *
     * @return BadgeInterface|Badge
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get Image
     *
     * @return array|mixed|string|null
     */
    public function getImg()
    {
        return $this->getData(self::IMG);
    }

    /**
     * Set Image
     *
     * @param $img
     *
     * @return BadgeInterface|Badge
     */
    public function setImg($img)
    {
        return $this->setData(self::IMG, $img);
    }

    /**
     * Get Identifier
     *
     * @return array|mixed|string|null
     */
    public function getIdentifier()
    {
        return $this->getData(self::IDENTIFIER);
    }

    /**
     * Set Identifier
     *
     * @param $identifier
     *
     * @return BadgeInterface|Badge
     */
    public function setIdentifier($identifier)
    {
        return $this->setData(self::IDENTIFIER, $identifier);
    }
}
