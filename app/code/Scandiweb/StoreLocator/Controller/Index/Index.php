<?php
/**
 * Scandiweb_StoreFinder
 *
 * @category    Scandiweb
 * @package     Scandiweb/StoreLocator
 * @author      Sandis Bogacenkovs <sandis.bogacenkovs@scandiweb.com>
 * @copyright   Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Scandiweb\StoreLocator\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\App\Action\Context;

/**
 * Class Index
 *
 * @package Scandiweb\StoreLocator\Controller\Index
 */
class Index extends Action
{
    /**
     * Index constructor.
     *
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Execute function
     *
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        /** @var Page $page */
        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $page->getConfig()->getTitle()->set(__('Store Locator'));

        return $page;
    }
}
