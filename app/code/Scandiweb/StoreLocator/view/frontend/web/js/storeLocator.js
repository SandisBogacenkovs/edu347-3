/**
 * Scandiweb_StoreFinder
 *
 * @category    Scandiweb
 * @package     Scandiweb/StoreFinder
 * @author      Sandis Bogacenkovs <sandis.bogacenkovs@scandiweb.com>
 * @copyright   Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

const FadeTime = 1;
const DefaultZoom = 7;
const StoreZoom = 13;

require([
    'jquery',
    'leafletMaps',
    'markercluster',
    'fullScreenMode',
    'esrileaflet',
    'esrigeocoder'
], function ($, LeafletMaps, MarkerCluster, FullScreenMode, EsriLeaflet, EsriGeocoder) {
    'use strict';

    L.esri = EsriLeaflet;
    L.esri.Geocoding = EsriGeocoder;
    $(document).ready(function () {
        $.ajaxSetup({async: false});
        let stores = null;
        let currentPos = {};

        $.ajax({
            url: "/storefinder/ajax/Mapstorelist",
            dataFormat: 'json',
            data: {
                page: 1,
                distance: 4000,
                lat: 56.897871,
                lon: 22.8079435,
                bounds: {
                    'lat_max': 89.99438063510406,
                    'lng_max': 180,
                    'lat_min': -89.06007300428232,
                    'lng_min': -180
                },
                lastindex: 1,
                loadMore: true
            },
            success: function (data) {
                stores = data.stores;
            }
        });

        let grayscale = L.tileLayer(
            'https://api.maptiler.com/maps/positron/{z}/{x}/{y}.png?key=CVSprbmR7lykdWDLILbp', {
                tileSize: 512,
                zoomOffset: -1,
                attribution: ''
            }),
            streets = L.tileLayer(
                'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    tileSize: 512,
                    zoomOffset: -1,
                    attribution: ''
                });

        let map = L.map('map', {
            center: [56.9655788, 24.1302559],
            minZoom: 2,
            maxZoom: 18,
            zoom: DefaultZoom,
            layers: [grayscale, streets],
            fullscreenControl: true,
            scrollWheelZoom: false
        });

        let baseMaps = {
            "Satellite": streets,
            "Map": grayscale
        };

        L.control.layers(baseMaps).addTo(map);

        stores.forEach(function (data) {
            L.marker([data.latitude, data.longitude]).bindPopup('<strong>' + data.store_name
                + '</strong><br>Working Hours: '
                + data.store_hours);

            $(".list").append('<div id=' + data.store_id
                + ' class="store-button"><b>'
                + data.store_name + '</b><br/>'
                + data.address + '</div>');
        });

        $("#search-storefinder").on('keyup', function () {
            showSuggestions($(this).val());
        });

        $("#search-storefinder").on('click', function () {
            showSuggestions($(this).val());
        });

        $(".list").on('click', '.store-button', function () {
            let store = stores.find(x => x.store_id === $(this).attr('id'));

            if (typeof store !== 'undefined') {
                $("#search-storefinder").val(store.address);
                flyToStore(store);
            }
        });

        $(".suggestion-container").on('click', '.suggestions', function () {
            let searchStr = getStoreAddress($(this).attr('id'));
            let store = stores.find(x => x.address === searchStr);

            $("#search-storefinder").val(searchStr);
            if (typeof store !== 'undefined')
                flyToStore(store);
            else {
                let results = getAddressLocation(searchStr);
                flyToAddress(results);
            }

        });

        $(".search-button").on('click', function () {
            let searchStr = $("#search-storefinder").val();
            let store = stores.find(x => x.address === searchStr);

            if (typeof store !== 'undefined') {
                flyToStore(store);
            } else {
                let results = getAddressLocation(searchStr);
                flyToAddress(results);
            }
        });

        $(document).on('click', function (e) {
            if (!$(e.target).hasClass('place-search'))
                $(".suggestion-container").empty().hide();
        });

        function showSuggestions(searchVal) {
            let storeSearch = null;

            if (searchVal.length > 1) {
                storeSearch = stores.filter(function (value) {
                    if (typeof value.address !== 'undefined') {
                        return value.address.toLowerCase().indexOf(searchVal.toLowerCase()) > -1;
                    }
                });
                $(".suggestion-container").empty().show();
                storeSearch.forEach(function (data) {
                    $(".suggestion-container").append('<div id=' + data.store_id + ' class="suggestions"><b>' + data.store_name + '</b><br/>' + data.address + '</div><br>')
                });
            } else
                $(".suggestion-container").empty().hide();
        }

        function getStoreAddress(id) {
            let store = stores.find(x => x.store_id === id);
            return store.address;
        }

        function flyToAddress(results) {
            let firstResult = results[0];

            currentPos.longitude = firstResult.lon;
            currentPos.latitude = firstResult.lat;
            calcDistance();
            sortStoreList();

            const firstAvailableStore = stores[0];
            map.flyTo([firstAvailableStore.latitude, firstAvailableStore.longitude], StoreZoom, {
                animate: true,
                duration: FadeTime
            });
        }

        function flyToStore(store) {
            map.flyTo([store.latitude, store.longitude], StoreZoom, {
                animate: true,
                duration: FadeTime
            });
            currentPos.longitude = store.longitude;
            currentPos.latitude = store.latitude;
            calcDistance();
            sortStoreList();
        }

        window.locateCountry = function (country) {
            L.esri.Geocoding.geocode().text(country).run(function (err, results, response) {
                let geoposition = results.results[0].latlng;
                let stateCode = results.results[0].properties;

                switch (stateCode.Country) {
                    case ("GBR"):
                        map.flyTo([geoposition.lat, geoposition.lng], 5, {
                            animate: true,
                            duration: FadeTime
                        })
                        calcDistance();
                        sortStoreList();
                        break;
                    case ("SWE"):
                        map.flyTo([geoposition.lat, geoposition.lng], 4, {
                            animate: true,
                            duration: FadeTime
                        })
                        break;
                    case ("USA"):
                        map.flyTo([geoposition.lat, geoposition.lng], 2, {
                            animate: true,
                            duration: FadeTime
                        })
                        break;
                    default:
                        map.flyTo([geoposition.lat, geoposition.lng], DefaultZoom, {
                            animate: true,
                            duration: FadeTime
                        })
                        break;
                }
                document.getElementById("search-storefinder").value = "";
            });
        }

        function calcDistance() {
            stores.forEach(function (data) {
                const R = 6371e3;

                let f1 = data.latitude * (Math.PI / 180);
                let f2 = currentPos.latitude * (Math.PI / 180);
                let lat = (data.latitude - currentPos.latitude) * (Math.PI / 180);
                let long = (data.longitude - currentPos.longitude) * (Math.PI / 180);
                let a = Math.sin(lat / 2) * Math.sin(lat / 2) + Math.cos(f1) * Math.cos(f2) * Math.sin(long / 2) * Math.sin(long / 2);
                let c = 2 * Math.atan(Math.sqrt(a), Math.sqrt(1 - a));
                data.distance = R * c;
            });
        }

        function sortStoreList() {
            stores.sort(function (a, b) {
                return a.distance - b.distance;
            });
            $(".list").empty();
            stores.forEach(function (data) {
                $(".list").append('<div id=' + data.store_id
                    + ' class="store-button"><b>' + data.store_name
                    + '</b><br/> ' + data.address + '</div>');
            });
        }

        function getAddressLocation(searchStr) {
            let result = {};
            $.get(location.protocol +
                '//nominatim.openstreetmap.org/search?format=json&q='
                + searchStr, function (data) {
                result = data;
            });
            return result;
        }

        let interval = null;

        function startGrouping() {
            interval = setInterval(runGrouping, 200);
        }

        function runGrouping() {
            let markers = L.markerClusterGroup();

            for (let i = 0; i < stores.length; i++) {
                let a = stores[i];
                let hours = a['store_hours'];
                let title = a['store_name'];
                let marker = L.marker(new L.LatLng(a['latitude'], a['longitude']), {
                    title: title
                });
                marker.bindPopup(hours);
                markers.addLayer(marker);
            }
            map.addLayer(markers);
            clearInterval(interval);
            interval = 0;
        }

        startGrouping();
    });
});
