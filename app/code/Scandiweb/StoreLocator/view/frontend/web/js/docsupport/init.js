/**
 * Scandiweb_StoreFinder
 *
 * @category    Scandiweb
 * @package     Scandiweb/StoreFinder
 * @author      Sandis Bogacenkovs <sandis.bogacenkovs@scandiweb.com>
 * @copyright   Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

var config = {
    '.chosen-select': {},
    '.chosen-select-deselect': {allow_single_deselect: true},
    '.chosen-select-no-single': {disable_search_threshold: 10},
    '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
    '.chosen-select-rtl': {rtl: true},
    '.chosen-select-width': {width: '95%'}
}
jQuery(document).ready(function ($) {
    let initializeInterval;

    function runInterval() {
        initializeInterval = setInterval(startChosen, 200);
    }

    function startChosen() {
        for (var selector in config) {
            $(selector).chosen(config[selector]);
            clearInterval(initializeInterval);
        }
    }
    runInterval();
});
