/**
 * Scandiweb_StoreFinder
 *
 * @category    Scandiweb
 * @package     Scandiweb/StoreFinder
 * @author      Sandis Bogacenkovs <sandis.bogacenkovs@scandiweb.com>
 * @copyright   Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

var config = {
    map: {
        "*": {
            leafletMaps: 'Scandiweb_StoreLocator/js/leaflet/leaflet',
            markercluster: 'Scandiweb_StoreLocator/js/markercluster/leaflet-markercluster-src',
            fullScreenMode: 'Scandiweb_StoreLocator/js/plugins/fullscreen',
            storeLocator: 'Scandiweb_StoreLocator/js/storeLocator',
            esrileaflet: 'Scandiweb_StoreLocator/js/plugins/esri-leaflet',
            esrigeocoder: 'Scandiweb_StoreLocator/js/plugins/esri-geocoder',
            chosen: 'Scandiweb_StoreLocator/js/plugins/chosen.jquery',
            choseninit: 'Scandiweb_StoreLocator/js/docsupport/init',

        }
    }
}
