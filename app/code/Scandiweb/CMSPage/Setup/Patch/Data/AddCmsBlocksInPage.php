<?php

namespace Scandiweb\CMSPage\Setup\Patch\Data;

use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\BlockRepository;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\Driver\File as FileAlias;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

/**
 * Class AddCmsBlocksInPage
 * @package Scandiweb\CMSPage\Setup\Patch\Data
 */
class AddCmsBlocksInPage implements DataPatchInterface
{
    /**
     * Constant data blocks used for loop
     */
    const BLOCKS = [
        'courier-delivery-service' => 'Courier Delivery Service',
        'half-column-content' => 'Half Column Content',
        'local-delivery-service' => 'Local Delivery Service',
        'local-delivery-w-table' => 'Local Delivery w table',
        'most-important-info' => 'Most Important Info'
    ];

    /**
     * @var BlockFactory
     */
    protected $blockFactory;

    /**
     * @var FileAlias
     */
    protected $driverFile;

    /**
     * @var BlockRepository
     */
    protected $blockRepository;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var File
     */
    protected $filesystemIo;

    /**
     * @var DirectoryList
     */
    protected $directoryList;


    /**
     * @param BlockFactory $blockFactory
     * @param BlockRepository $blockRepository
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param FileAlias $driverFile
     * @param File $filesystemIo
     * @param DirectoryList $directoryList
     */
    public function __construct(
        BlockFactory $blockFactory,
        BlockRepository $blockRepository,
        ModuleDataSetupInterface $moduleDataSetup,
        FileAlias $driverFile,
        File $filesystemIo,
        DirectoryList $directoryList
    ) {
        $this->blockFactory = $blockFactory;
        $this->blockRepository = $blockRepository;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->driverFile = $driverFile;
        $this->filesystemIo = $filesystemIo;
        $this->directoryList = $directoryList;
    }

    /**
     * Upgrade.
     * @throws FileSystemException
     * @throws CouldNotSaveException
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $this->createCmsBlocks();
        $this->moveMediaFiles();
        $this->moduleDataSetup->endSetup();
    }

    /**
     * @throws CouldNotSaveException
     * @throws FileSystemException
     */
    private function createCmsBlocks()
    {
        $appDir = $this->directoryList->getPath(DirectoryList::APP);
        foreach (self::BLOCKS as $blockIdentifier => $blockTitle) {
            $data = [
                'title' => $blockTitle,
                'identifier' => $blockIdentifier,
                'stores' => ['0'],
                'is_active' => 1,
                'content' => $this->driverFile->fileGetContents($appDir . DIRECTORY_SEPARATOR . 'code/Scandiweb/CMSPage/files/cms/blocks/' . $blockIdentifier . '.html')
            ];

            $newBlock = $this->blockFactory->create();
            $newBlock->setData($data);
            $this->blockRepository->save($newBlock);
        }
    }

    /**
     * @throws FileSystemException
     */
    public function moveMediaFiles()
    {
        $mediaDir = $this->directoryList->getPath(DirectoryList::MEDIA);
        $designDir = $this->directoryList->getPath(DirectoryList::APP);
        $filePath = $designDir . DIRECTORY_SEPARATOR . 'design/frontend/ScandiCustom/CustomTemplate/web/assets/images' . DIRECTORY_SEPARATOR;// source folder
        $copyFileFullPath = $mediaDir . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR; //
        $content = glob($filePath . '*');

        foreach ($content as $value) {
            $this->filesystemIo->cp($value, $copyFileFullPath);
        }
    }

    /**
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }
}
