<?php

namespace Scandiweb\CMSPage\Setup;

use Magento\Cms\Model\PageFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Class UpgradeData
 * @package Scandiweb\CMSPage\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * Construct
     *
     * @param PageFactory $pageFactory
     */
    public function __construct(
        PageFactory $pageFactory
    ) {
        $this->pageFactory = $pageFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.2.1') < 0) {
            $cmsPage = $this->pageFactory->create()->load('cms-page', 'identifier');
            $data = ['content' => file_get_contents('Scandiweb_CMSPage/files/cms/pages/index.html')];
            $cmsPage->setContent($data['content'])->save();

        }
        $setup->endSetup();
    }
}
