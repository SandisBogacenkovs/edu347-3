<?php

namespace Scandiweb\CMSPage\Setup;

use Magento\Cms\Model\BlockFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

//use Magento\Cms\Model\PageFactory;

/**
 * Class InstallData
 * @package Scandiweb\CMSPage\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * InstallData constructor.
     * @param BlockFactory $blockFactory
     */
    public function __construct(
        BlockFactory $blockFactory
    ) {
        $this->blockFactory = $blockFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Exception
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $testBlock = [
            'title' => 'Test block title',
            'identifier' => 'test-block',
            'stores' => [0],
            'is_active' => 1,
        ];
        $this->blockFactory->create()->setData($testBlock)->save();
    }
}
