<?php
/**
 * ProductList_Related
 *
 * @category    ProductList
 * @package     ProductList_Related
 * @author      Sandis Bogacenkovs <sandis.bogacenkovs@scandiweb.com>
 * @copyright   Copyright (c) 2020 Scandiweb, Inc (http://scandiweb.com)
 */

namespace Scandiweb\SlickSlider\Block\Product\ProductList;

use Magento\Catalog\Block\Product\ProductList\Related as MagentoRelated;


/**
 * Class Related
 * @package Scandiweb\SlickSlider\Block\Product\ProductList
 */
class Related extends MagentoRelated
{

    /**
     * Set related item limit to 10
     */
    const RELATED_LIMIT = 10;

    /**
     * Prepare data
     *
     * @return $this
     */
    protected function _prepareData()
    {
        $product = $this->getProduct();
        /* @var $product Product */

        $this->_itemCollection = $product->getRelatedProductCollection()->addAttributeToSelect(
            'required_options'
        )->setPositionOrder()->addStoreFilter();

        //limit number of related products
        $this->_itemCollection->setPageSize(self::RELATED_LIMIT);

        if ($this->moduleManager->isEnabled('Magento_Checkout')) {
            $this->_addProductAttributesAndPrices($this->_itemCollection);
        }
        $this->_itemCollection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());

        $this->_itemCollection->load();

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }
}
