<?php

namespace Scandiweb\SlickSlider\Setup\Patch\Data;

use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\PageFactory;
use Magento\Cms\Model\BlockRepository;
use Magento\Cms\Model\PageRepository;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\Driver\File as FileAlias;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;


/**
 * Class SlickContentPatch
 * @package Scandiweb\SlickSlider\Setup\Patch\Data
 */
class SlickContentPatch implements DataPatchInterface
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;
    /**
     * @var BlockFactory
     */
    protected $blockFactory;

    /**
     * @var FileAlias
     */
    protected $driverFile;

    /**
     * @var BlockRepository
     */
    protected $blockRepository;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var DirectoryList
     */
    protected $directoryList;

    /**
     * @var PageRepository
     */
    private $pageRepository;

    /**
     * @param BlockFactory $blockFactory
     * @param BlockRepository $blockRepository
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param FileAlias $driverFile
     * @param DirectoryList $directoryList
     * @param PageFactory $pageFactory
     * @param PageRepository $pageRepository
     */
    public function __construct(
        BlockFactory $blockFactory,
        BlockRepository $blockRepository,
        ModuleDataSetupInterface $moduleDataSetup,
        FileAlias $driverFile,
        DirectoryList $directoryList,
        PageFactory $pageFactory,
        PageRepository $pageRepository
    ) {
        $this->pageFactory = $pageFactory;
        $this->blockFactory = $blockFactory;
        $this->blockRepository = $blockRepository;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->driverFile = $driverFile;
        $this->directoryList = $directoryList;
        $this->pageFactory = $pageFactory;
        $this->pageRepository = $pageRepository;
    }

    /**
     * @return SlickContentPatch|void
     * @throws FileSystemException
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $this->createPage();
        $this->moduleDataSetup->endSetup();
    }

    /**
     * @throws CouldNotSaveException
     * @throws FileSystemException
     */
    private function createPage()
    {
        $appDir = $this->directoryList->getPath(DirectoryList::APP);
        $content = $appDir . DIRECTORY_SEPARATOR . 'code/Scandiweb/SlickSlider/files/cms/pages/index.html';
        $data = [
            'title' => 'Slick Slider',
            'identifier' => 'slick-slider',
            'stores' => ['0'],
            'is_active' => 1,
            'page_layout' => '1column',
            'content' => $this->driverFile->fileGetContents($content)
        ];

        $cmsPage = $this->pageFactory->create();
        $cmsPage->setData($data);
        $this->pageRepository->save($cmsPage);
    }

    /**
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }
}
