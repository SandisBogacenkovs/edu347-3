<?php


/**
 * Copyright © 2017 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

\Magento\Framework\COmponent\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/ScandiCustom/CustomTemplate',
    __DIR__
);
