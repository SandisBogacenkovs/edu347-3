/**
 * Scandiweb_StickyHeader
 *
 * @category    StickyHeader
 * @package     Scandiweb/StickyHeader
 * @author      Sandis Bogacenkovs <sandis.bogacenkovs@scandiweb.com>
 * @copyright   Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

var config = {
    map: {
        'stickyheader': {
            sticky: 'js/stickyheader.js',
        }
    }
};
