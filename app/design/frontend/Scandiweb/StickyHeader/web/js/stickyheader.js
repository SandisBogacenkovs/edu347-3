/**
 * StickyHeader  Configuration and animation controls
 *
 * @category    StickyHeader
 * @package     Scandiweb/StickyHeader
 * @author      Sandis Bogacenkovs <sandis.bogacenkovs@scandiweb.com>
 * @copyright   Copyright (c) 2020 Scandiweb, Ltd (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

require(['jquery'],
    function ($) {
        'use strict';

        let topSpacing = $('.content.wrapper').height();
        let shrink = {top: -topSpacing};
        const stickyHeader = "sticky-header";
        const pageHeader = ".page-header";
        const stickyHeaderMobile = "sticky-header-mobile";
        const headerContainers = ".logo, .minicart-wrapper, .block-search"
        const animateTop = "80px";
        const tablet = 768;
        const topOffSet = 400;
        if ($(window).width() >= tablet) {
            $(window).scroll(function () {
                if ($(window).scrollTop() >= topOffSet) {
                    if ($(pageHeader).hasClass(stickyHeader) === false) {
                        $(pageHeader).addClass(stickyHeader).css(shrink);
                        $(headerContainers).animate({top: animateTop})
                    }
                } else if ($(window).scrollTop() <= topOffSet) {
                    if ($(pageHeader).hasClass(stickyHeader) === true) {
                        $(pageHeader).removeClass(stickyHeader).removeAttr('style');
                        $(headerContainers).animate({top: 0})

                    }
                }
            });
        } else {
            // Can apply custom styling for devices with screen size > 767px
            $(window).scroll(function () {
                if ($(window).scrollTop() >= topOffSet) {
                    if ($(pageHeader).hasClass(stickyHeader) === false) {
                        $(pageHeader).addClass(stickyHeaderMobile).css(shrink);
                    }
                } else if ($(window).scrollTop() <= topOffSet) {
                    if ($(pageHeader).hasClass(stickyHeaderMobile) === true) {
                        $(pageHeader).removeClass(stickyHeaderMobile).removeAttr('style');
                    }
                }
            });
        }
    });
