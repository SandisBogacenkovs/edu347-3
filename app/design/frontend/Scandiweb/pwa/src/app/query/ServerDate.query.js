/**
 * ScandiPWA_ServerDate
 *
 * @category  ScandiPWA
 * @package   ScandiPWA_ServerDate
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

import {Field} from 'Util/Query';

class ServerDate {

    getServerDateQuery(options) {
        const {sliderId} = options;

        return new Field('scandiwebSlider')
            .addArgument('id', 'ID!', sliderId)
            .addField(this._getDateField())
            .setAlias('serverDate');
    }

    _getDateField() {
        return new Field('slides')
            .addField('server_date');
    }
}

export default new ServerDate();
