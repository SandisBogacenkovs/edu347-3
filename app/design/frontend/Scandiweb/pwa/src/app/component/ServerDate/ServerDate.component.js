/**
 * ScandiPWA_ServerDate
 *
 * @category  ScandiPWA
 * @package   ScandiPWA_ServerDate
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

import {PureComponent} from 'react';
import PropTypes from 'prop-types';
import './ServerDate.style';

class ServerDate extends PureComponent {
    static propTypes = {
        serverDate: PropTypes.string.isRequired
    };

    render() {
        const {serverDate} = this.props;

        return (
            <div block="ServerDate">
                <div className="ServerDate-text">
                    {`The current time on server is : ${serverDate}`};
                </div>
            </div>
        );
    }
}

export default ServerDate;
