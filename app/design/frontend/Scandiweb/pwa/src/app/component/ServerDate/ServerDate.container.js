/**
 * ScandiPWA_ServerDate
 *
 * @category  ScandiPWA
 * @package   ScandiPWA_ServerDate
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

import {PureComponent} from 'react';
import ServerDate from './ServerDate.component';
import ServerDateQuery from 'Query/ServerDate.query';
import {prepareQuery} from 'Util/Query';
import {executeGet} from 'Util/Request';
import {ONE_MONTH_IN_SECONDS} from 'Util/Request/QueryDispatcher';

class ServerDateContainer extends PureComponent {
    state = {
        serverDate: {server_date: '12/12/12'}
    };

    constructor(props) {
        super(props);
        this.requestServerDate();
    }

    requestServerDate() {
        const readyQuery = prepareQuery([ServerDateQuery.getServerDateQuery(this.props)]);
        executeGet(readyQuery, 'serverDate', ONE_MONTH_IN_SECONDS).then(
            ({serverDate}) => this.setState({serverDate}),
            e => console.log(e)
        );
    }

    render() {
        const {serverDate} = this.state;
        const {slideId} = this.props;

        if (serverDate['slides'] == undefined)
            return (<span/>)
        return (
            <ServerDate serverDate={serverDate['slides'][slideId]['server_date']}/>
        );
    }
}

export default ServerDateContainer;
