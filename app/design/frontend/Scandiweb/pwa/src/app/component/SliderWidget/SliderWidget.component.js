/**
 * ScandiPWA_ServerDate
 *
 * @category  ScandiPWA
 * @package   ScandiPWA_ServerDate
 * @author    Sandis Bogacenkovs sandis.bogacenkovs@scandiweb.com
 * @copyright Copyright (c) 2020 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

import SourceSliderWidgetComponent from 'SourceComponent/SliderWidget/SliderWidget.component';
import Html from 'Component/Html';
import Image from 'Component/Image';
import ServerDate from "Component/ServerDate";

export default class SliderWidget extends SourceSliderWidgetComponent {
    renderServerDate(slide_id) {
        const {sliderId} = this.props;
        return <ServerDate sliderId={sliderId} slideId={slide_id}/>
    }

    renderSlide = (slide, i) => {
        const {
            slide_text,
            isPlaceholder,
            title: block
        } = slide;

        return (
            <figure
                block="SliderWidget"
                elem="Figure"
                key={i}
            >
                {this.renderServerDate(i)}
                <Image
                    mix={{block: 'SliderWidget', elem: 'FigureImage'}}
                    ratio="custom"
                    src={this.getSlideImage(slide)}
                    isPlaceholder={isPlaceholder}
                />
                <figcaption
                    block="SliderWidget"
                    elem="Figcaption"
                    mix={{block}}
                >
                    <Html content={slide_text || ''}/>
                </figcaption>
            </figure>
        );
    };
}
