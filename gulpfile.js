'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');


sass.compiler = require('node-sass');

gulp.task('scss', function () {
    return gulp.src('./app/design/frontend/ScandiCustom/CustomTemplate/web/assets/scss/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./app/design/frontend/ScandiCustom/CustomTemplate/web/css'));
});

gulp.task('slick', function () {
    return gulp.src('./app/design/frontend/ScandiCustom/CustomTemplate/web/assets/scss/slick.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename('slick.css'))
        .pipe(gulp.dest('./app/design/frontend/ScandiCustom/CustomTemplate/web/css'));
});

gulp.task('scdefault', function () {
    return gulp.src('./app/design/frontend/scandi/default/web/assets/scss/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename('defscandi.css'))
        .pipe(gulp.dest('./app/design/frontend/scandi/default/web/css'));
});

gulp.task('badge', function () {
    return gulp.src('./app/code/Scandiweb/Badge/view/frontend/web/assets/scss/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename('badgestyles.css'))
        .pipe(gulp.dest('./app/code/Scandiweb/Badge/view/frontend/web/css'));
});
